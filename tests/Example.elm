module Example exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)


suite : Test
suite =
    describe "Example test suite"
        [ test "has no effect on a palindrome" <|
            \_ ->
                let
                    palindrome =
                        "hannah"
                in
                Expect.equal palindrome (String.reverse palindrome)
        , fuzzWith { runs = 350 } string "fuzzy test example" <|
            \randomString ->
                randomString
                    |> String.reverse
                    |> String.reverse
                    |> Expect.equal randomString
        ]
