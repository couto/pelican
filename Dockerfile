FROM node:carbon-slim
LABEL maintainer="Luis Couto <hello@luiscouto.pt>"

RUN yarn global add elm@0.18.0
RUN yarn global add elm-test@^0.18.0
RUN yarn global add elm-live@^2.7.5
RUN yarn global add elm-format@exp

