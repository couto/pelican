module Main exposing (..)


type AuthMethod
    = AuthDummy
    | AuthPassword


toAuthString : AuthMethod -> String
toAuthString s =
    case s of
        AuthDummy ->
            "m.login.dummy"

        AuthPassword ->
            "m.login.password"


type alias Login =
    { username : String
    , password : String
    , auth : { type_ : AuthMethod }
    }
