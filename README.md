

## How to run a development server

```
docker-compose up -d
```

You can find more info at the [Synapse repository](https://github.com/matrix-org/synapse/tree/master/contrib/docker)

